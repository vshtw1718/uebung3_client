package de.htw.saar;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;

public class Uebung3Client {

    private static final Integer PORT = 8080;

    public static void main (String[] args){
        String limit          = (args.length > 0) ? args[0] : "8";
        String baseUrl        = (args.length > 1) ? args[1] : "http://localhost:"+PORT.toString();
        String webContextPath = "/uebung3/getPrimeNumbers";
        System.out.println("\nAngefragte URL: " + baseUrl + webContextPath + "?n=" + limit);

        Client c = ClientBuilder.newClient();
        WebTarget target = c.target(baseUrl);

        System.out.println( "\nText output:" );
        System.out.println(target.path("/uebung3/getPrimeNumbersAsInteger")
                .queryParam("n", limit)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class));

        System.out.println( "\nText output:" );
        System.out.println(target.path(webContextPath)
                .queryParam("n", limit)
                .request(MediaType.TEXT_PLAIN)
                .get(String.class));

        System.out.println( "\nText output:" );
        System.out.println(target.path(webContextPath)
                .queryParam("n", limit)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class));
    }
}
